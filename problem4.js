function findCarYear(inventory) {
    if (inventory.length === 0) {
        return []
    }
    else {
        let  carYearArray  = []
        for (let i = 0 ; i < inventory.length ; i++) {
            let car = inventory[i].car_year ;
            carYearArray.push(car)
        }
        return carYearArray.sort()
    }
}

module.exports = findCarYear ;