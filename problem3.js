function carModels(inventory) {
    if (inventory.length === 0) {
        return []
    }
    else {
        let  carModelArray  = []
        for (let i = 0 ; i < inventory.length ; i++) {
            let model = inventory[i].car_model ;
            carModelArray.push(model)
        }
        return carModelArray.sort()
    }
}

module.exports = carModels ;