function findLastCar(inventory) {
    if (inventory.length === 0 ) {
        return []
    }
    else {
        let lastCarIndex = inventory.length - 1 ;
        return [inventory[lastCarIndex].car_make, inventory[lastCarIndex].car_model]
    }
}
module.exports = findLastCar ;