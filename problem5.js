function findOldCars(carYearArray) {
    if (carYearArray.length === 0) {
        return []
    }
    else {
        let olderCarsArray = []
        for (i = 0 ; i < carYearArray.length ; i++) {
            if (carYearArray[i] < 2000) {
                olderCarsArray.push(carYearArray[i])
            }
        }
        return olderCarsArray
    }
}

module.exports = findOldCars ;