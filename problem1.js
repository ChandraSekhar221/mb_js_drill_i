function findCar(inventory,searchId ) {
    if ((typeof(searchId) === 'undefined') || (inventory.length === 0)) {
        return []
    }
    if (inventory.length > 0) {
        for (let i = 0 ; i < inventory.length ; i++) {
            if ( inventory[i].id === searchId) {
                return inventory[i]
            }
        }
    } 
}
module.exports = findCar ;