const findLastCar = require('../problem2');
const inventory = require('../inventory');
const result = findLastCar(inventory);
if (result.length === 0) {
    console.log('Please provide the inventory data..')
} else {
    console.log(`Last Car is a ${result[0]} ${result[1]}`)
}